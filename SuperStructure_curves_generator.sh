#!/bin/bash
  
#############################################
#   SuperStructure ANALYSIS FOR REAL DATA   #
#############################################

# Base directory
MYBASEDIR=`pwd`

# USER DEFINED PROCESSORS
# Number of processors to be used
n_proc=1

#---------------------------------------------

# USER DEFINED EPSILON
# Starting/final/increment epsilon
epsi=0
epsf=10
inc=1

# Data folder
data_folder="Data"

# Input data file (.dat format)
data_file=$1

# SuperStructure curves file
output_file="SuperStructure.${data_file}"

#---------------------------------------------

##########################################################################
# PART 1: Cluster analysis with DBSCAN progressively increasiing epsilon #
##########################################################################

# Enter in the working directory
cd ${MYBASEDIR}/${data_folder}

# Create working directory
mkdir -p ${MYBASEDIR}/${data_folder}/analysis_superstructure/

# Check number of points
npoints=$(awk '{n++}END{print n}' ${data_file}) 
npoints=$(($npoints-1)) #to account for header

# Iteration variable to count how many processors we are using
i=0

# Print the number of points
echo "Processing file ${data}..."
echo "Processing $npoints points "

# Ieration over R parameter of dbscan
for eps in $(seq ${epsi} ${inc} ${epsf})
do 
  
  #cluster file 
  clfile="CLUSTERS.eps_${eps}.${data_file}"

  # Increase processor counter
  i=$((${i}+1))

  # Print the R we are analysing
  echo "Doing epsilon = ${eps} .. "
  
  # Dbscan analysis for given R
  awk -v np="${npoints}" -v eps="${eps}" 'BEGIN{print(eps,0,np);}{if(NR>1)print(int($1),$3,$4,0.0);}' ${data_file} | ${MYBASEDIR}/mydbscan > analysis_superstructure/${clfile} & 

  var=$((${i}%${n_proc}))
  
  if [ "${var}" -eq "0" ]; then
    wait
  fi

done

# Wait for all processes to finish
wait


###############################################
# PART 2: Generation of SuperStructure Curves #
###############################################

echo "Generation of SuperStructure Curves..."

# Create SuperStructure curves file with header
echo "#Epsilon Nclusters Ncluster_norm" > ${output_file}

# Cycle over epsilon parameter
for eps in $(seq ${epsi} ${inc} ${epsf})
do

  echo "Doing epsilon = ${eps}"

  #cluster file 
  clfile="CLUSTERS.eps_${eps}.${data_file}"

  # Number of clusters
  max=$(awk 'BEGIN{max=-1;}{if(NR>5 && $5>max)max=$5}END{print (max+1)}' analysis_superstructure/${clfile})
  awk -v ep="${eps}" -v ma="${max}" -v np="${npoints}" 'BEGIN{print ep, ma, ma/np}' >> ${output_file}

done
